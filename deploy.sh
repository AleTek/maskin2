#!/bin/sh
mvn clean package
COLOR=$(sh get_next_color.sh)
docker build -t webapp-$COLOR .
docker-compose stop  webapp-$COLOR
docker-compose rm -f  webapp-$COLOR
docker-compose up -d --scale webapp-$COLOR=2
